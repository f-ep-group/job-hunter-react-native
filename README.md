# Job Hunter

A React native application where developers can browse, search and apply for jobs.
## 🚀 How to use

```sh
npm run start
```

## 🚀 THE UI of the application

<img src="screenshots/IMG_2665.PNG" width="300">
<img src="screenshots/IMG_2666.PNG" width="300">
<img src="screenshots/IMG_2667.PNG" width="300">
<img src="screenshots/IMG_2668.PNG" width="300">
<img src="screenshots/IMG_2669.PNG" width="300">
<img src="screenshots/IMG_2670.PNG" width="300">



## 📝 Notes

- [Expo Router: Docs](https://expo.github.io/router)
- [Expo Router: Repo](https://github.com/expo/router)
